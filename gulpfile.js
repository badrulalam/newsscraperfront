var gulp = require('gulp'),
inject = require('gulp-inject'),  
runSequence = require('run-sequence'),
browserSync = require('browser-sync').create();


gulp.task('bs-reload',function(){
  browserSync.reload();
});

gulp.task('inject',function(cb){
 runSequence(
    'bs-reload',
    cb);
 });

gulp.task('watch', function() {
  browserSync.init({
    open:false,
    reloadDelay: 1000,
    browser: "chrome",
    server: "."
   });
  gulp.watch('myApp/**/*',['inject']);  
  });


gulp.task('default',['inject']);
