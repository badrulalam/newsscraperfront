'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    ['$rootScope', '$state', '$stateParams',
      function($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
      }
    ]
  )
  .config(
    ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG',
      function($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {
        var layout = "tpl/app.html";
        layout = "tpl/blocks/material.layout.html";
        $urlRouterProvider
          .otherwise('/app/news');

        $stateProvider
          .state('app', {
            abstract: true,
            url: '/app',
            templateUrl: layout
          })
          .state('app.news', {
            url: '/news',
            templateUrl: 'tpl/apps_news.html',
            resolve: load(['js/controllers/news.js'])
          })
          .state('app.facebook', {
            url: '/facebook',
            templateUrl: 'tpl/app_dashboard_v3.html',
            resolve: load(['js/controllers/chart.js'])
          })
          .state('app.browse-news', {
            url: '/browse-news',
            templateUrl: 'tpl/browse-news.html',
            resolve: load(['moment', 'js/controllers/browse-news.js'])
          })
          .state('app.scraper-log', {
            url: '/scraper-log',
            templateUrl: 'tpl/scraper-log.html',
            resolve: load(['moment','ui.grid', 'js/controllers/scraper-log.js'])
          })
          .state('app.browse-facebook', {
            url: '/browse-facebook',
            templateUrl: 'tpl/app_dashboard_v3.html',
            resolve: load(['js/controllers/chart.js'])
          })

            .state('app.zepage-one', {
              url: '/zepage-one',
              templateUrl: 'tpl/zepage-one.html',
              controller: "DthreeCtrl",
              resolve: load(['js/controllers/dthree.js'])
            })




        function load(srcs, callback) {
          return {
            deps: ['$ocLazyLoad', '$q',
              function($ocLazyLoad, $q) {
                var deferred = $q.defer();
                var promise = false;
                srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                if (!promise) {
                  promise = deferred.promise;
                }
                angular.forEach(srcs, function(src) {
                  promise = promise.then(function() {
                    if (JQ_CONFIG[src]) {
                      return $ocLazyLoad.load(JQ_CONFIG[src]);
                    }
                    angular.forEach(MODULE_CONFIG, function(module) {
                      if (module.name == src) {
                        name = module.name;
                      } else {
                        name = src;
                      }
                    });
                    return $ocLazyLoad.load(name);
                  });
                });
                deferred.resolve();
                return callback ? promise.then(function() {
                  return callback();
                }) : promise;
              }
            ]
          }
        }


      }
    ]
  );
