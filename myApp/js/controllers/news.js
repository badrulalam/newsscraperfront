'use strict';
/* Controllers */
app.controller('NewsCtrl', ['$scope', '$http','Settings', function($scope, $http,Settings) {
  var _this = this;
  var enumerateDaysBetweenDates = function(startDate, endDate) {
    /**
     * Returns range of dates including start and end
     */
    var dates = [startDate.toDate()];
    var currDate = startDate.clone().startOf('day');
    var lastDate = endDate.clone().startOf('day');

    while (currDate.add(1, 'days').diff(lastDate) < 0) {
      dates.push(currDate.clone().toDate());
    }
    dates.push(endDate.toDate());
    return dates;
  };

  _.assign(_this, {
    dates: '2016-09-22 - 2016-09-22', // Debug only
    searchNews: function() {
      var scrap_endpoint = Settings.apiBase+'/links';
      /**
       * prepare the links to scrape here and send.
       * No point making it in server side.
       */
      var dates = _this.dates.split(" - ");
      var totalDates = enumerateDaysBetweenDates(moment(dates[0]), moment(dates[1]));
      var links = [];
      _.chain(_this.sources).filter({
        selected: true
      }).each(function(v) {
        _.map(totalDates, function(vv) {
          v.archives_url = v.archives_url.replace('{DATE}', moment(vv).format(v.date_format));
          links.push(v);
        });
      }).value();
      $http.post(scrap_endpoint, links).then(function(response) {
        alert(response.data.jobs + " Jobs Added");
      });
    }
  });
  $http.get("data/scraper-schema.json").then(function(response) {
    _.assign(_this, {
      sources: _.map(response.data, function(v) {
        v.selected = true;
        return v
      })
    });
  });
}]);
