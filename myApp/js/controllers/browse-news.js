'use strict'
/* Controllers */
app.controller('NewsBrowserCtrl', ['$scope', '$http','Settings', function($scope, $http,Settings) {
  var _this = this;
  $scope.$watch('vm.sources',function(newValue,oldValue){
    if(newValue!==oldValue){
      /**
       * Fire data table API with these filters
       */
      _.chain(newValue).filter({'selected':true}).size().value();
      _this.dataOptions.iDisplayLength=10;
    }
  },true);
  _.assign(_this, {
    dataOptions: {
      'iDisplayLength': 25,
      'serverSide': true,
      sAjaxSource: Settings.apiBase+'/browse-news',
      'deferRender': true,
      aoColumns: [{
        mData: 'source'
      }, {
        mData: 'url',
        'render': function(data, type, row, meta) {
          return '<a target="_blank" class="btn btn-default btn-xs btn-info" href="' +
          data + '">Link</a>'
        }
      }, {
        mData: 'date',
        render: function(data) {
          return moment(data).format('MMM DD, YYYY')
        }
      }, {
        mData: 'title'
      }, {
        mData: 'content',
        render: function(data) {
          return _.truncate(data, {
            length: 150
          })
        }
      }]
    }
  });
  $http.get('data/scraper-schema.json').then(function(response) {
    _.assign(_this, {
      sources: _.map(response.data, function(v) {
        v.selected = true
        return v
      })
    })
  })
}]);
