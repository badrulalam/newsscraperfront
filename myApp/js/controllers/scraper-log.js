'use strict'
/* Controllers */
app.controller('ScraperLogCtrl', ['$scope', '$http','Settings', function($scope, $http,Settings) {  
  var _this = this;  
  $http.get(Settings.apiBase+'/get-log').then(function(response) {            
    _.assign(_this, {      
        data : _.map(response.data.data,function(v){
          v.date = moment(v._id['year']+"-"+v._id['month']+"-"+v._id['day']+"-","YYYY-MM-DD").format("MMM DD, YYYY");
          v.sources = v.source.join("<br />");
          return v;
        })
    })
  })
}]);
